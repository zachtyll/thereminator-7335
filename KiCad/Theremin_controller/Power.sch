EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R?
U 1 1 5C6244FD
P 2350 3300
AR Path="/5C6244FD" Ref="R?"  Part="1" 
AR Path="/5C61E9F8/5C6244FD" Ref="R5"  Part="1" 
F 0 "R5" H 2420 3346 50  0000 L CNN
F 1 "330" H 2420 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2280 3300 50  0001 C CNN
F 3 "~" H 2350 3300 50  0001 C CNN
	1    2350 3300
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C?
U 1 1 5C62450D
P 3750 2900
AR Path="/5C62450D" Ref="C?"  Part="1" 
AR Path="/5C61E9F8/5C62450D" Ref="C16"  Part="1" 
F 0 "C16" H 3865 2946 50  0000 L CNN
F 1 "100n" H 3865 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3788 2750 50  0001 C CNN
F 3 "~" H 3750 2900 50  0001 C CNN
	1    3750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2700 3750 2750
Connection ~ 3750 2700
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C624517
P 3750 3100
AR Path="/5C624517" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C624517" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 3750 2850 50  0001 C CNN
F 1 "GND" H 3755 2927 50  0000 C CNN
F 2 "" H 3750 3100 50  0001 C CNN
F 3 "" H 3750 3100 50  0001 C CNN
	1    3750 3100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:CP-Device-Lab1-rescue C?
U 1 1 5C62451D
P 3350 2900
AR Path="/5C62451D" Ref="C?"  Part="1" 
AR Path="/5C61E9F8/5C62451D" Ref="C15"  Part="1" 
F 0 "C15" H 3468 2946 50  0000 L CNN
F 1 "10u" H 3468 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_5x5.4" H 3388 2750 50  0001 C CNN
F 3 "~" H 3350 2900 50  0001 C CNN
	1    3350 2900
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C624524
P 3350 3100
AR Path="/5C624524" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C624524" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 3350 2850 50  0001 C CNN
F 1 "GND" H 3355 2927 50  0000 C CNN
F 2 "" H 3350 3100 50  0001 C CNN
F 3 "" H 3350 3100 50  0001 C CNN
	1    3350 3100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C?
U 1 1 5C62452A
P 2850 2900
AR Path="/5C62452A" Ref="C?"  Part="1" 
AR Path="/5C61E9F8/5C62452A" Ref="C14"  Part="1" 
F 0 "C14" H 2965 2946 50  0000 L CNN
F 1 "100n" H 2965 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2888 2750 50  0001 C CNN
F 3 "~" H 2850 2900 50  0001 C CNN
	1    2850 2900
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C624531
P 2850 3100
AR Path="/5C624531" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C624531" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 2850 2850 50  0001 C CNN
F 1 "GND" H 2855 2927 50  0000 C CNN
F 2 "" H 2850 3100 50  0001 C CNN
F 3 "" H 2850 3100 50  0001 C CNN
	1    2850 3100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+5V-power-Lab1-rescue #PWR?
U 1 1 5C624537
P 2850 2650
AR Path="/5C624537" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C624537" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 2850 2500 50  0001 C CNN
F 1 "+5V" H 2865 2823 50  0000 C CNN
F 2 "" H 2850 2650 50  0001 C CNN
F 3 "" H 2850 2650 50  0001 C CNN
	1    2850 2650
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R?
U 1 1 5C62455B
P 5200 3500
AR Path="/5C62455B" Ref="R?"  Part="1" 
AR Path="/5C61E9F8/5C62455B" Ref="R6"  Part="1" 
F 0 "R6" H 5270 3546 50  0000 L CNN
F 1 "220" H 5270 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5130 3500 50  0001 C CNN
F 3 "~" H 5200 3500 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C624562
P 5200 3750
AR Path="/5C624562" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C624562" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 5200 3500 50  0001 C CNN
F 1 "GND" H 5205 3577 50  0000 C CNN
F 2 "" H 5200 3750 50  0001 C CNN
F 3 "" H 5200 3750 50  0001 C CNN
	1    5200 3750
	1    0    0    -1  
$EndComp
Connection ~ 2850 2700
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C?
U 1 1 5C625BC4
P 5700 2900
AR Path="/5C625BC4" Ref="C?"  Part="1" 
AR Path="/5C61E9F8/5C625BC4" Ref="C17"  Part="1" 
F 0 "C17" H 5815 2946 50  0000 L CNN
F 1 "100n" H 5815 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5738 2750 50  0001 C CNN
F 3 "~" H 5700 2900 50  0001 C CNN
	1    5700 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2700 5700 2750
Wire Wire Line
	5700 2700 6200 2700
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR?
U 1 1 5C625BCD
P 6200 2650
AR Path="/5C625BCD" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C625BCD" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 6200 2500 50  0001 C CNN
F 1 "+3.3V" H 6215 2823 50  0000 C CNN
F 2 "" H 6200 2650 50  0001 C CNN
F 3 "" H 6200 2650 50  0001 C CNN
	1    6200 2650
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:CP-Device-Lab1-rescue C?
U 1 1 5C625BD3
P 6200 2900
AR Path="/5C625BD3" Ref="C?"  Part="1" 
AR Path="/5C61E9F8/5C625BD3" Ref="C18"  Part="1" 
F 0 "C18" H 6318 2946 50  0000 L CNN
F 1 "10u" H 6318 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_5x5.4" H 6238 2750 50  0001 C CNN
F 3 "~" H 6200 2900 50  0001 C CNN
	1    6200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2750 6200 2700
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C625BDD
P 6200 3100
AR Path="/5C625BDD" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C625BDD" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 6200 2850 50  0001 C CNN
F 1 "GND" H 6205 2927 50  0000 C CNN
F 2 "" H 6200 3100 50  0001 C CNN
F 3 "" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C625BE3
P 5700 3100
AR Path="/5C625BE3" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C625BE3" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 5700 2850 50  0001 C CNN
F 1 "GND-power" H 5705 2927 50  0000 C CNN
F 2 "" H 5700 3100 50  0001 C CNN
F 3 "" H 5700 3100 50  0001 C CNN
	1    5700 3100
	1    0    0    -1  
$EndComp
Connection ~ 5700 2700
Wire Wire Line
	5200 3650 5200 3750
Wire Wire Line
	5200 2850 5200 2700
Connection ~ 5200 2700
Wire Wire Line
	5200 2700 5700 2700
Wire Wire Line
	2850 2700 3350 2700
Wire Wire Line
	3350 2750 3350 2700
Connection ~ 3350 2700
Wire Wire Line
	3350 2700 3750 2700
Wire Wire Line
	2850 2750 2850 2700
Wire Wire Line
	2850 3100 2850 3050
Wire Wire Line
	3350 3100 3350 3050
Wire Wire Line
	3750 3100 3750 3050
Wire Wire Line
	6200 3100 6200 3050
Wire Wire Line
	5700 3050 5700 3100
Wire Wire Line
	6200 2700 6200 2650
Connection ~ 6200 2700
Wire Wire Line
	2850 2700 2850 2650
Text HLabel 2100 2700 0    50   Input ~ 0
USB-Power
$Comp
L Theremin_controller-rescue:LED-device D3
U 1 1 5C649F01
P 2350 2950
F 0 "D3" V 2388 2833 50  0000 R CNN
F 1 "LED" V 2297 2833 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2350 2950 50  0001 C CNN
F 3 "~" H 2350 2950 50  0001 C CNN
	1    2350 2950
	0    -1   -1   0   
$EndComp
$Comp
L Theremin_controller-rescue:LED-device D4
U 1 1 5C64A075
P 5200 3000
F 0 "D4" V 5238 2883 50  0000 R CNN
F 1 "LED" V 5147 2883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5200 3000 50  0001 C CNN
F 3 "~" H 5200 3000 50  0001 C CNN
	1    5200 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 2700 2350 2700
Wire Wire Line
	2350 2700 2350 2800
Connection ~ 2350 2700
Wire Wire Line
	2350 2700 2850 2700
Wire Wire Line
	2350 3100 2350 3150
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C64B6B0
P 2350 3500
AR Path="/5C64B6B0" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C64B6B0" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 2350 3250 50  0001 C CNN
F 1 "GND" H 2355 3327 50  0000 C CNN
F 2 "" H 2350 3500 50  0001 C CNN
F 3 "" H 2350 3500 50  0001 C CNN
	1    2350 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3450 2350 3500
Wire Wire Line
	5200 3150 5200 3350
$Comp
L Theremin_controller-rescue:LM1117-3.3-regul U2
U 1 1 5C6453B5
P 4350 2700
F 0 "U2" H 4350 3158 50  0000 C CNN
F 1 "LM1117-3.3" H 4350 3067 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 4350 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 4350 2976 50  0000 C CNN
	1    4350 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2700 4050 2700
Wire Wire Line
	4650 2700 5200 2700
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C645C70
P 4350 3050
AR Path="/5C645C70" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C645C70" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 4350 2800 50  0001 C CNN
F 1 "GND" H 4355 2877 50  0000 C CNN
F 2 "" H 4350 3050 50  0001 C CNN
F 3 "" H 4350 3050 50  0001 C CNN
	1    4350 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3000 4350 3050
$EndSCHEMATC
