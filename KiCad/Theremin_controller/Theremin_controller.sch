EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Theremin_controller-rescue:STM32F411RETx-MCU_ST_STM32F4-Lab1-rescue U1
U 1 1 5C49D14F
P 5100 3850
F 0 "U1" H 5100 1964 50  0000 C CNN
F 1 "STM32F411RETx" H 5100 1873 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 4500 2150 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00115249.pdf" H 5100 3850 50  0001 C CNN
	1    5100 3850
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C6
U 1 1 5C49D521
P 2500 3550
F 0 "C6" H 2615 3596 50  0000 L CNN
F 1 "100n" H 2615 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2538 3400 50  0001 C CNN
F 3 "~" H 2500 3550 50  0001 C CNN
	1    2500 3550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C4
U 1 1 5C49D528
P 2050 3550
F 0 "C4" H 2165 3596 50  0000 L CNN
F 1 "100n" H 2165 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2088 3400 50  0001 C CNN
F 3 "~" H 2050 3550 50  0001 C CNN
	1    2050 3550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C3
U 1 1 5C49D52F
P 1600 3550
F 0 "C3" H 1715 3596 50  0000 L CNN
F 1 "100n" H 1715 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1638 3400 50  0001 C CNN
F 3 "~" H 1600 3550 50  0001 C CNN
	1    1600 3550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR01
U 1 1 5C49D9F1
P 1600 3250
F 0 "#PWR01" H 1600 3100 50  0001 C CNN
F 1 "+3.3V" H 1615 3423 50  0000 C CNN
F 2 "" H 1600 3250 50  0001 C CNN
F 3 "" H 1600 3250 50  0001 C CNN
	1    1600 3250
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR09
U 1 1 5C49DED2
P 5100 1700
F 0 "#PWR09" H 5100 1550 50  0001 C CNN
F 1 "+3.3V" H 5115 1873 50  0000 C CNN
F 2 "" H 5100 1700 50  0001 C CNN
F 3 "" H 5100 1700 50  0001 C CNN
	1    5100 1700
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR02
U 1 1 5C49E25E
P 1600 3850
F 0 "#PWR02" H 1600 3600 50  0001 C CNN
F 1 "GND" H 1605 3677 50  0000 C CNN
F 2 "" H 1600 3850 50  0001 C CNN
F 3 "" H 1600 3850 50  0001 C CNN
	1    1600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3700 2500 3750
Wire Wire Line
	2050 3700 2050 3750
Wire Wire Line
	1600 3700 1600 3750
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR010
U 1 1 5C49EB3F
P 6750 4550
F 0 "#PWR010" H 6750 4300 50  0001 C CNN
F 1 "GND" H 6755 4377 50  0000 C CNN
F 2 "" H 6750 4550 50  0001 C CNN
F 3 "" H 6750 4550 50  0001 C CNN
	1    6750 4550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C2
U 1 1 5C4A01FD
P 1150 3550
F 0 "C2" H 1265 3596 50  0000 L CNN
F 1 "100n" H 1265 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1188 3400 50  0001 C CNN
F 3 "~" H 1150 3550 50  0001 C CNN
	1    1150 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3700 1150 3750
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C1
U 1 1 5C4A1884
P 700 3550
F 0 "C1" H 815 3596 50  0000 L CNN
F 1 "4.7µ" H 815 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 738 3400 50  0001 C CNN
F 3 "~" H 700 3550 50  0001 C CNN
	1    700  3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  3700 700  3750
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R1
U 1 1 5C49DE3F
P 3350 2350
F 0 "R1" V 3143 2350 50  0000 C CNN
F 1 "10K" V 3234 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3280 2350 50  0001 C CNN
F 3 "~" H 3350 2350 50  0001 C CNN
	1    3350 2350
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:+3V3-power-Lab1-rescue #PWR06
U 1 1 5C4A203A
P 3100 2250
F 0 "#PWR06" H 3100 2100 50  0001 C CNN
F 1 "+3V3" H 3000 2400 50  0000 L CNN
F 2 "" H 3100 2250 50  0001 C CNN
F 3 "" H 3100 2250 50  0001 C CNN
	1    3100 2250
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R2
U 1 1 5C4A2EC0
P 4250 2550
F 0 "R2" V 4043 2550 50  0000 C CNN
F 1 "10K" V 4134 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4180 2550 50  0001 C CNN
F 3 "~" H 4250 2550 50  0001 C CNN
	1    4250 2550
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR07
U 1 1 5C4A8CE5
P 3900 2700
F 0 "#PWR07" H 3900 2450 50  0001 C CNN
F 1 "GND" V 3905 2572 50  0000 R CNN
F 2 "" H 3900 2700 50  0001 C CNN
F 3 "" H 3900 2700 50  0001 C CNN
	1    3900 2700
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:Conn_01x06_Male-Connector-Lab1-rescue J1
U 1 1 5C4AF7AA
P 1150 6850
F 0 "J1" H 1256 7228 50  0000 C CNN
F 1 "Conn_01x06_Male" H 1256 7137 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1150 6850 50  0001 C CNN
F 3 "~" H 1150 6850 50  0001 C CNN
	1    1150 6850
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C10
U 1 1 5C4A01EF
P 6200 1350
F 0 "C10" H 6315 1396 50  0000 L CNN
F 1 "100n" H 6315 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 1200 50  0001 C CNN
F 3 "~" H 6200 1350 50  0001 C CNN
	1    6200 1350
	-1   0    0    1   
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C11
U 1 1 5C4A01F6
P 6650 1350
F 0 "C11" H 6765 1396 50  0000 L CNN
F 1 "100n" H 6765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6688 1200 50  0001 C CNN
F 3 "~" H 6650 1350 50  0001 C CNN
	1    6650 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 1200 6200 1200
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR011
U 1 1 5C4B78A0
P 6200 1500
F 0 "#PWR011" H 6200 1250 50  0001 C CNN
F 1 "GND" H 6205 1327 50  0000 C CNN
F 2 "" H 6200 1500 50  0001 C CNN
F 3 "" H 6200 1500 50  0001 C CNN
	1    6200 1500
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR013
U 1 1 5C4B78E5
P 6650 1500
F 0 "#PWR013" H 6650 1250 50  0001 C CNN
F 1 "GND" H 6655 1327 50  0000 C CNN
F 2 "" H 6650 1500 50  0001 C CNN
F 3 "" H 6650 1500 50  0001 C CNN
	1    6650 1500
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR012
U 1 1 5C4B7931
P 6650 1150
F 0 "#PWR012" H 6650 1000 50  0001 C CNN
F 1 "+3.3V" H 6665 1323 50  0000 C CNN
F 2 "" H 6650 1150 50  0001 C CNN
F 3 "" H 6650 1150 50  0001 C CNN
	1    6650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 6650 1850 6650
Wire Wire Line
	1850 6650 1850 6400
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR03
U 1 1 5C4BBEE9
P 1850 6350
F 0 "#PWR03" H 1850 6200 50  0001 C CNN
F 1 "+3.3V" H 1865 6523 50  0000 C CNN
F 2 "" H 1850 6350 50  0001 C CNN
F 3 "" H 1850 6350 50  0001 C CNN
	1    1850 6350
	1    0    0    -1  
$EndComp
Text Label 1900 6950 0    50   ~ 0
SWDI0
Text Label 1900 7050 0    50   ~ 0
NRST
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C5
U 1 1 5C4BEBE0
P 2350 6700
F 0 "C5" H 2465 6746 50  0000 L CNN
F 1 "100n" H 2465 6655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2388 6550 50  0001 C CNN
F 3 "~" H 2350 6700 50  0001 C CNN
	1    2350 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 6550 2350 6400
Wire Wire Line
	2350 6400 1850 6400
Connection ~ 1850 6400
Wire Wire Line
	1850 6400 1850 6350
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR04
U 1 1 5C4C0C8E
P 2800 7050
F 0 "#PWR04" H 2800 6800 50  0001 C CNN
F 1 "GND" V 2805 6922 50  0000 R CNN
F 2 "" H 2800 7050 50  0001 C CNN
F 3 "" H 2800 7050 50  0001 C CNN
	1    2800 7050
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device-Lab1-rescue C9
U 1 1 5C4C5067
P 4100 2950
F 0 "C9" H 4215 2996 50  0000 L CNN
F 1 "4.7u" H 4215 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4138 2800 50  0001 C CNN
F 3 "~" H 4100 2950 50  0001 C CNN
	1    4100 2950
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR08
U 1 1 5C4C62B6
P 4100 3100
F 0 "#PWR08" H 4100 2850 50  0001 C CNN
F 1 "GND" H 4105 2927 50  0000 C CNN
F 2 "" H 4100 3100 50  0001 C CNN
F 3 "" H 4100 3100 50  0001 C CNN
	1    4100 3100
	1    0    0    -1  
$EndComp
NoConn ~ 5800 2950
NoConn ~ 5800 3050
NoConn ~ 5800 3850
NoConn ~ 5800 4250
NoConn ~ 5800 4450
NoConn ~ 5800 4550
NoConn ~ 5800 4650
NoConn ~ 5800 4750
NoConn ~ 5800 4850
NoConn ~ 5800 4950
NoConn ~ 5800 5050
NoConn ~ 5800 5150
NoConn ~ 5800 5250
NoConn ~ 5800 5350
NoConn ~ 5800 5450
NoConn ~ 4400 5450
NoConn ~ 4400 5350
NoConn ~ 4400 5250
NoConn ~ 4400 5150
NoConn ~ 4400 5050
NoConn ~ 4400 4950
NoConn ~ 4400 4850
NoConn ~ 4400 4750
NoConn ~ 4400 4650
NoConn ~ 4400 4550
NoConn ~ 4400 4450
NoConn ~ 4400 4350
NoConn ~ 4400 4250
NoConn ~ 4400 4150
NoConn ~ 4400 3750
Wire Wire Line
	4900 2150 4900 1750
Wire Wire Line
	5000 2150 5000 1750
Wire Wire Line
	4900 1750 5000 1750
Connection ~ 5000 1750
Wire Wire Line
	5000 1750 5100 1750
Wire Wire Line
	5100 2150 5100 1750
Connection ~ 5100 1750
Wire Wire Line
	5200 2150 5200 1750
Wire Wire Line
	5200 1750 5100 1750
Wire Wire Line
	5300 2150 5300 1750
Wire Wire Line
	5300 1750 5200 1750
Connection ~ 5200 1750
Wire Wire Line
	4900 5650 4900 6050
Wire Wire Line
	5000 5650 5000 6050
Wire Wire Line
	4900 6050 5000 6050
Connection ~ 5000 6050
Wire Wire Line
	5000 6050 5100 6050
Connection ~ 5100 6050
Wire Wire Line
	5200 5650 5200 6050
Wire Wire Line
	5200 6050 5100 6050
Wire Wire Line
	5300 6050 5200 6050
Connection ~ 5200 6050
Wire Wire Line
	5100 5650 5100 6050
Wire Wire Line
	4400 2750 4100 2750
Wire Wire Line
	1350 6850 2350 6850
Connection ~ 2350 6850
Wire Wire Line
	2350 6850 2800 6850
Wire Wire Line
	1350 6950 1900 6950
Wire Wire Line
	1350 7050 1900 7050
Wire Wire Line
	1900 7150 1350 7150
Wire Wire Line
	6200 1200 6650 1200
Connection ~ 6200 1200
Connection ~ 6650 1200
Text Label 1900 6750 0    50   ~ 0
SWCLK
Wire Wire Line
	1350 6750 1900 6750
Text Label 1900 7150 0    50   ~ 0
SW0
Text Label 5800 3650 0    50   ~ 0
SWDI0
Text Label 5800 3750 0    50   ~ 0
SWCLK
Text Label 5800 4350 0    50   ~ 0
SW0
Text Label 3900 2350 0    50   ~ 0
NRST
Wire Wire Line
	700  3350 1150 3350
Wire Wire Line
	700  3350 700  3400
Connection ~ 1150 3350
Wire Wire Line
	1150 3350 1150 3400
Wire Wire Line
	1150 3350 1600 3350
Connection ~ 1600 3350
Wire Wire Line
	1600 3350 1600 3400
Wire Wire Line
	1600 3350 2050 3350
Connection ~ 2050 3350
Wire Wire Line
	2050 3350 2050 3400
Wire Wire Line
	2050 3350 2500 3350
Wire Wire Line
	2500 3350 2500 3400
Wire Wire Line
	2050 3750 2500 3750
Connection ~ 2050 3750
Wire Wire Line
	2050 3750 1600 3750
Connection ~ 1600 3750
Wire Wire Line
	1150 3750 1600 3750
Connection ~ 1150 3750
Wire Wire Line
	1150 3750 700  3750
Wire Wire Line
	1600 3750 1600 3850
Wire Wire Line
	1600 3350 1600 3250
$Sheet
S 700  4200 650  550 
U 5C60A54E
F0 "INPUTS" 50
F1 "inputs.sch" 50
F2 "AMP_INPUT" O R 1350 4350 50 
F3 "FREQ_INPUT" O R 1350 4500 50 
$EndSheet
Wire Wire Line
	5300 5650 5300 6050
$Sheet
S 7850 850  1550 750 
U 5C61E9F8
F0 "Power" 50
F1 "Power.sch" 50
F2 "USB-Power" I L 7850 1150 50 
$EndSheet
Wire Wire Line
	4100 2750 4100 2800
Wire Wire Line
	3900 2700 3900 2550
Wire Wire Line
	3900 2550 4100 2550
Wire Wire Line
	3100 2250 3100 2350
Wire Wire Line
	3100 2350 3200 2350
Wire Wire Line
	5100 1750 5100 1700
Wire Wire Line
	6650 1200 6650 1150
Wire Wire Line
	2800 6850 2800 7050
Wire Wire Line
	5100 6100 5100 6050
Wire Wire Line
	5400 1200 5400 2150
$Sheet
S 850  1700 750  850 
U 5C6AD1F6
F0 "USB" 50
F1 "USB.sch" 50
F2 "USB-Power" O R 1600 1800 50 
F3 "RXD-out" I R 1600 2000 50 
F4 "TXD-out" O R 1600 2150 50 
F5 "RTS-out" I R 1600 2300 50 
F6 "CTS-out" O R 1600 2450 50 
$EndSheet
$Sheet
S 8600 3700 600  350 
U 5C668B48
F0 "PWM_OUTPUTS" 50
F1 "pwm_outputs.sch" 50
F2 "SOUND_PWM" I L 8600 3750 50 
F3 "BLUE_PWM" I L 8600 3850 50 
F4 "RED_PWM" I L 8600 3950 50 
$EndSheet
Text Label 5800 2350 0    50   ~ 0
FREQ_IN
Text Label 5800 2450 0    50   ~ 0
AMP_IN
Text Label 5800 2550 0    50   ~ 0
SOUND_PWM
Text Label 5800 2650 0    50   ~ 0
BLUE_PWM
Text Label 5800 2850 0    50   ~ 0
RED_PWM
Text Label 8100 3750 0    50   ~ 0
SOUND_PWM
Wire Wire Line
	8100 3750 8600 3750
Wire Wire Line
	8100 3850 8600 3850
Wire Wire Line
	8100 3950 8600 3950
Text Label 8100 3850 0    50   ~ 0
BLUE_PWM
Text Label 8100 3950 0    50   ~ 0
RED_PWM
Wire Wire Line
	1750 4350 1350 4350
Wire Wire Line
	1350 4500 1750 4500
Text Label 1350 4500 0    50   ~ 0
FREQ_IN
Text Label 1350 4350 0    50   ~ 0
AMP_IN
NoConn ~ 5800 2750
NoConn ~ 5800 3150
Text Label 5800 3250 0    50   ~ 0
TXD
Text Label 5800 3350 0    50   ~ 0
RXD
Text Label 5800 3450 0    50   ~ 0
CTS
Text Label 5800 3550 0    50   ~ 0
RTS
Text Label 1600 2000 0    50   ~ 0
TXD
Text Label 1600 2150 0    50   ~ 0
RXD
Text Label 1600 2300 0    50   ~ 0
CTS
Text Label 1600 2450 0    50   ~ 0
RTS
Text Label 1600 1800 0    50   ~ 0
USB-Power
Text Label 7400 1150 0    50   ~ 0
USB-Power
Wire Wire Line
	7400 1150 7850 1150
NoConn ~ 4400 3450
NoConn ~ 4400 3550
Wire Wire Line
	3500 2350 4400 2350
Connection ~ 3500 2350
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR041
U 1 1 5C6487AE
P 3500 3350
F 0 "#PWR041" H 3500 3100 50  0001 C CNN
F 1 "GND" H 3505 3177 50  0000 C CNN
F 2 "" H 3500 3350 50  0001 C CNN
F 3 "" H 3500 3350 50  0001 C CNN
	1    3500 3350
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:LED-device D2
U 1 1 5C64BD66
P 6750 4250
F 0 "D2" V 6788 4133 50  0000 R CNN
F 1 "LED" V 6697 4133 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6750 4250 50  0001 C CNN
F 3 "~" H 6750 4250 50  0001 C CNN
	1    6750 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 3250 3500 3300
$Comp
L Theremin_controller-rescue:SW_Push-Switch SW2
U 1 1 5C652E0A
P 3500 3050
F 0 "SW2" V 3546 3002 50  0000 R CNN
F 1 "SW_Push" V 3455 3002 50  0000 R CNN
F 2 "ThereminLib:SW_MEC_5GSH9" H 3500 3250 50  0001 C CNN
F 3 "" H 3500 3250 50  0001 C CNN
	1    3500 3050
	0    -1   -1   0   
$EndComp
$Comp
L Theremin_controller-rescue:R-device R16
U 1 1 5C655AFB
P 6150 4050
F 0 "R16" V 5943 4050 50  0000 C CNN
F 1 "220" V 6034 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6080 4050 50  0001 C CNN
F 3 "~" H 6150 4050 50  0001 C CNN
	1    6150 4050
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R14
U 1 1 5C657FE0
P 3400 4550
F 0 "R14" V 3193 4550 50  0000 C CNN
F 1 "4.7k" V 3284 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3330 4550 50  0001 C CNN
F 3 "~" H 3400 4550 50  0001 C CNN
	1    3400 4550
	-1   0    0    1   
$EndComp
$Comp
L Theremin_controller-rescue:R-Device-Lab1-rescue R12
U 1 1 5C658085
P 3100 4550
F 0 "R12" V 2893 4550 50  0000 C CNN
F 1 "4.7k" V 2984 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3030 4550 50  0001 C CNN
F 3 "~" H 3100 4550 50  0001 C CNN
	1    3100 4550
	-1   0    0    1   
$EndComp
$Comp
L Theremin_controller-rescue:SW_DIP_x02-Switch SW1
U 1 1 5C658126
P 3200 5000
F 0 "SW1" V 3154 5230 50  0000 L CNN
F 1 "SW_DIP_x02" V 3245 5230 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_DIP_SPSTx02_Slide_Copal_CHS-02A_W5.08mm_P1.27mm_JPin" H 3200 5000 50  0001 C CNN
F 3 "" H 3200 5000 50  0001 C CNN
	1    3200 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 4700 3200 4700
Wire Wire Line
	3300 4700 3400 4700
Text Label 3500 4700 0    50   ~ 0
AUDIO_ON
Connection ~ 3400 4700
Text Label 2700 4700 0    50   ~ 0
MIDI_ON
Wire Wire Line
	2700 4700 3100 4700
Connection ~ 3100 4700
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR039
U 1 1 5C6643DE
P 3100 5650
F 0 "#PWR039" H 3100 5400 50  0001 C CNN
F 1 "GND" H 3105 5477 50  0000 C CNN
F 2 "" H 3100 5650 50  0001 C CNN
F 3 "" H 3100 5650 50  0001 C CNN
	1    3100 5650
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR040
U 1 1 5C664415
P 3400 5650
F 0 "#PWR040" H 3400 5400 50  0001 C CNN
F 1 "GND" H 3405 5477 50  0000 C CNN
F 2 "" H 3400 5650 50  0001 C CNN
F 3 "" H 3400 5650 50  0001 C CNN
	1    3400 5650
	1    0    0    -1  
$EndComp
Text Label 4100 3950 0    50   ~ 0
MIDI_ON
Wire Wire Line
	4100 3950 4400 3950
Text Label 4050 4050 0    50   ~ 0
AUDIO_ON
Wire Wire Line
	4050 4050 4400 4050
Wire Wire Line
	5800 4050 6000 4050
Wire Wire Line
	6750 4400 6750 4550
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR042
U 1 1 5C672450
P 5100 6100
F 0 "#PWR042" H 5100 5850 50  0001 C CNN
F 1 "GND" H 5105 5927 50  0000 C CNN
F 2 "" H 5100 6100 50  0001 C CNN
F 3 "" H 5100 6100 50  0001 C CNN
	1    5100 6100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR047
U 1 1 5C62D1B9
P 3100 4300
F 0 "#PWR047" H 3100 4150 50  0001 C CNN
F 1 "+3.3V" H 3115 4473 50  0000 C CNN
F 2 "" H 3100 4300 50  0001 C CNN
F 3 "" H 3100 4300 50  0001 C CNN
	1    3100 4300
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+3.3V-power-Lab1-rescue #PWR048
U 1 1 5C62D1F0
P 3400 4300
F 0 "#PWR048" H 3400 4150 50  0001 C CNN
F 1 "+3.3V" H 3415 4473 50  0000 C CNN
F 2 "" H 3400 4300 50  0001 C CNN
F 3 "" H 3400 4300 50  0001 C CNN
	1    3400 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4300 3100 4400
Wire Wire Line
	3400 4300 3400 4400
$Comp
L Theremin_controller-rescue:R-device R18
U 1 1 5C6334DB
P 6100 4150
F 0 "R18" V 5893 4150 50  0000 C CNN
F 1 "220" V 5984 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6030 4150 50  0001 C CNN
F 3 "~" H 6100 4150 50  0001 C CNN
	1    6100 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 4050 6750 4100
Wire Wire Line
	6300 4050 6750 4050
$Comp
L Theremin_controller-rescue:LED-device D5
U 1 1 5C6376A3
P 6400 4350
F 0 "D5" V 6438 4233 50  0000 R CNN
F 1 "LED" V 6347 4233 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6400 4350 50  0001 C CNN
F 3 "~" H 6400 4350 50  0001 C CNN
	1    6400 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 4150 5950 4150
Wire Wire Line
	6250 4150 6400 4150
Wire Wire Line
	6400 4150 6400 4200
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR049
U 1 1 5C63FC4C
P 6400 4600
F 0 "#PWR049" H 6400 4350 50  0001 C CNN
F 1 "GND" H 6405 4427 50  0000 C CNN
F 2 "" H 6400 4600 50  0001 C CNN
F 3 "" H 6400 4600 50  0001 C CNN
	1    6400 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4500 6400 4600
Wire Wire Line
	3500 2350 3500 2700
$Comp
L Theremin_controller-rescue:C-Device C27
U 1 1 5C640F1B
P 3050 3050
F 0 "C27" H 3165 3096 50  0000 L CNN
F 1 "100n" H 3165 3005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3088 2900 50  0001 C CNN
F 3 "~" H 3050 3050 50  0001 C CNN
	1    3050 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2700 3050 2700
Wire Wire Line
	3050 2700 3050 2900
Connection ~ 3500 2700
Wire Wire Line
	3500 2700 3500 2850
$Comp
L Theremin_controller-rescue:R-Device R13
U 1 1 5C643416
P 3300 3300
F 0 "R13" V 3093 3300 50  0000 C CNN
F 1 "100" V 3184 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3230 3300 50  0001 C CNN
F 3 "~" H 3300 3300 50  0001 C CNN
	1    3300 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 3300 3500 3300
Connection ~ 3500 3300
Wire Wire Line
	3500 3300 3500 3350
Wire Wire Line
	3050 3200 3050 3300
Wire Wire Line
	3050 3300 3150 3300
$Comp
L Theremin_controller-rescue:C-Device C26
U 1 1 5C64854A
P 2700 4950
F 0 "C26" H 2815 4996 50  0000 L CNN
F 1 "100n" H 2815 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2738 4800 50  0001 C CNN
F 3 "~" H 2700 4950 50  0001 C CNN
	1    2700 4950
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C28
U 1 1 5C6485EE
P 4000 5000
F 0 "C28" H 4115 5046 50  0000 L CNN
F 1 "100n" H 4115 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4038 4850 50  0001 C CNN
F 3 "~" H 4000 5000 50  0001 C CNN
	1    4000 5000
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:R-Device R9
U 1 1 5C64866C
P 2900 5300
F 0 "R9" V 2693 5300 50  0000 C CNN
F 1 "100" V 2784 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2830 5300 50  0001 C CNN
F 3 "~" H 2900 5300 50  0001 C CNN
	1    2900 5300
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:R-Device R15
U 1 1 5C6486FE
P 3650 5300
F 0 "R15" V 3443 5300 50  0000 C CNN
F 1 "100" V 3534 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3580 5300 50  0001 C CNN
F 3 "~" H 3650 5300 50  0001 C CNN
	1    3650 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 4700 2700 4800
Wire Wire Line
	2700 5100 2700 5300
Wire Wire Line
	2700 5300 2750 5300
Wire Wire Line
	3050 5300 3100 5300
Wire Wire Line
	3300 5300 3400 5300
Wire Wire Line
	3100 5300 3100 5650
Connection ~ 3100 5300
Wire Wire Line
	3100 5300 3200 5300
Wire Wire Line
	3400 5300 3400 5650
Connection ~ 3400 5300
Wire Wire Line
	3400 5300 3500 5300
Wire Wire Line
	4000 5150 4000 5300
Wire Wire Line
	3800 5300 4000 5300
Wire Wire Line
	4000 4850 4000 4700
Wire Wire Line
	3400 4700 4000 4700
$EndSCHEMATC
