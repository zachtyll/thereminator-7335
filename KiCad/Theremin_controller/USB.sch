EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Theremin_controller-rescue:USB_OTG-Connector-Lab1-rescue J?
U 1 1 5C6AD790
P 2150 2800
AR Path="/5C6AD790" Ref="J?"  Part="1" 
AR Path="/5C61E9F8/5C6AD790" Ref="J?"  Part="1" 
AR Path="/5C6AD1F6/5C6AD790" Ref="J2"  Part="1" 
F 0 "J2" H 2205 3267 50  0000 C CNN
F 1 "USB_OTG" H 2205 3176 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Lumberg_2486_01_Horizontal" H 2300 2750 50  0001 C CNN
F 3 " ~" H 2300 2750 50  0001 C CNN
	1    2150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3200 2150 3350
Wire Wire Line
	2150 3350 2100 3350
Wire Wire Line
	2050 3350 2050 3200
Wire Wire Line
	2100 3350 2100 3500
Connection ~ 2100 3350
Wire Wire Line
	2100 3350 2050 3350
$Comp
L Theremin_controller-rescue:GND-power-Lab1-rescue #PWR?
U 1 1 5C6AD79D
P 2100 3500
AR Path="/5C6AD79D" Ref="#PWR?"  Part="1" 
AR Path="/5C61E9F8/5C6AD79D" Ref="#PWR?"  Part="1" 
AR Path="/5C6AD1F6/5C6AD79D" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 2100 3250 50  0001 C CNN
F 1 "GND" H 2105 3327 50  0000 C CNN
F 2 "" H 2100 3500 50  0001 C CNN
F 3 "" H 2100 3500 50  0001 C CNN
	1    2100 3500
	1    0    0    -1  
$EndComp
Text HLabel 3350 1900 2    50   Output ~ 0
USB-Power
$Comp
L Theremin_controller-rescue:FT232RL-Interface_USB U3
U 1 1 5C6ADB32
P 4750 3200
F 0 "U3" H 4750 4378 50  0000 C CNN
F 1 "FT232RL" H 4750 4287 50  0000 C CNN
F 2 "Package_SO:SSOP-28_5.3x10.2mm_P0.65mm" H 4750 3200 50  0001 C CNN
F 3 "http://www.ftdichip.com/Products/ICs/FT232RL.htm" H 4750 3200 50  0001 C CNN
	1    4750 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2800 3000 2800
Wire Wire Line
	4750 4200 4750 4250
Wire Wire Line
	4750 4250 4850 4250
Wire Wire Line
	4850 4250 4850 4200
Wire Wire Line
	4950 4200 4950 4250
Wire Wire Line
	4950 4250 4850 4250
Connection ~ 4850 4250
$Comp
L Theremin_controller-rescue:GND-power #PWR029
U 1 1 5C6ADD27
P 4550 4350
F 0 "#PWR029" H 4550 4100 50  0001 C CNN
F 1 "GND" H 4555 4177 50  0000 C CNN
F 2 "" H 4550 4350 50  0001 C CNN
F 3 "" H 4550 4350 50  0001 C CNN
	1    4550 4350
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+5V-power #PWR032
U 1 1 5C6184CB
P 4750 1850
F 0 "#PWR032" H 4750 1700 50  0001 C CNN
F 1 "+5V-power" H 4765 2023 50  0000 C CNN
F 2 "" H 4750 1850 50  0001 C CNN
F 3 "" H 4750 1850 50  0001 C CNN
	1    4750 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2200 4850 1900
Wire Wire Line
	4850 1900 4750 1900
Wire Wire Line
	4750 1900 4750 1850
Wire Wire Line
	4650 2200 4650 1900
Wire Wire Line
	4650 1900 4750 1900
Connection ~ 4750 1900
NoConn ~ 3950 3200
NoConn ~ 3950 3400
NoConn ~ 3950 3600
NoConn ~ 5550 3600
NoConn ~ 5550 3700
NoConn ~ 5550 3200
NoConn ~ 5550 3100
NoConn ~ 5550 3000
NoConn ~ 5550 2900
Wire Wire Line
	4550 4200 4550 4250
Wire Wire Line
	4550 4250 4750 4250
Connection ~ 4750 4250
Wire Wire Line
	3950 3900 3850 3900
Wire Wire Line
	3850 3900 3850 4250
Wire Wire Line
	3850 4250 4550 4250
Connection ~ 4550 4250
Wire Wire Line
	4550 4250 4550 4350
$Comp
L Theremin_controller-rescue:C-Device C7
U 1 1 5C619C69
P 3700 2500
F 0 "C7" V 3448 2500 50  0000 C CNN
F 1 "100nF" V 3539 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3738 2350 50  0001 C CNN
F 3 "" H 3700 2500 50  0001 C CNN
	1    3700 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 2500 3850 2500
$Comp
L Theremin_controller-rescue:GND-power #PWR05
U 1 1 5C619F40
P 3400 2500
F 0 "#PWR05" H 3400 2250 50  0001 C CNN
F 1 "GND" H 3405 2327 50  0000 C CNN
F 2 "" H 3400 2500 50  0001 C CNN
F 3 "" H 3400 2500 50  0001 C CNN
	1    3400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2500 3400 2500
$Comp
L Theremin_controller-rescue:R-Device R11
U 1 1 5C61AF43
P 5850 3800
F 0 "R11" V 5643 3800 50  0000 C CNN
F 1 "10K" V 5734 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5780 3800 50  0001 C CNN
F 3 "" H 5850 3800 50  0001 C CNN
	1    5850 3800
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:+5V-power #PWR033
U 1 1 5C61B046
P 6200 3750
F 0 "#PWR033" H 6200 3600 50  0001 C CNN
F 1 "+5V-power" H 6215 3923 50  0000 C CNN
F 2 "" H 6200 3750 50  0001 C CNN
F 3 "" H 6200 3750 50  0001 C CNN
	1    6200 3750
	1    0    0    -1  
$EndComp
NoConn ~ 5550 3900
Wire Wire Line
	5550 3800 5700 3800
Text HLabel 6150 2600 2    50   Input ~ 0
RXD-out
Text HLabel 6150 2500 2    50   Output ~ 0
TXD-out
Text HLabel 6150 2700 2    50   Input ~ 0
RTS-out
Text HLabel 6150 2800 2    50   Output ~ 0
CTS-out
Wire Wire Line
	5550 2600 5650 2600
Wire Wire Line
	6150 2700 5550 2700
Wire Wire Line
	5550 2800 6150 2800
NoConn ~ 5550 3500
$Comp
L Theremin_controller-rescue:GND-power #PWR037
U 1 1 5C632C91
P 3050 4550
F 0 "#PWR037" H 3050 4300 50  0001 C CNN
F 1 "GND-power" H 3055 4377 50  0000 C CNN
F 2 "" H 3050 4550 50  0001 C CNN
F 3 "" H 3050 4550 50  0001 C CNN
	1    3050 4550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:+5V-power #PWR036
U 1 1 5C63310C
P 3050 4150
F 0 "#PWR036" H 3050 4000 50  0001 C CNN
F 1 "+5V-power" H 3065 4323 50  0000 C CNN
F 2 "" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-device C8
U 1 1 5C633169
P 2700 4350
F 0 "C8" H 2815 4396 50  0000 L CNN
F 1 "100nF" H 2815 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2738 4200 50  0001 C CNN
F 3 "~" H 2700 4350 50  0001 C CNN
	1    2700 4350
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:CP-device C20
U 1 1 5C633281
P 3350 4350
F 0 "C20" H 3468 4396 50  0000 L CNN
F 1 "4,7uF" H 3468 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3388 4200 50  0001 C CNN
F 3 "~" H 3350 4350 50  0001 C CNN
	1    3350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4500 3050 4500
Wire Wire Line
	3350 4200 3050 4200
Wire Wire Line
	3050 4550 3050 4500
Connection ~ 3050 4500
Wire Wire Line
	3050 4500 3350 4500
Wire Wire Line
	3050 4150 3050 4200
Connection ~ 3050 4200
Wire Wire Line
	3050 4200 2700 4200
$Comp
L Theremin_controller-rescue:CP-device C21
U 1 1 5C63753E
P 2700 3100
F 0 "C21" H 2818 3146 50  0000 L CNN
F 1 "10nF" H 2818 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2738 2950 50  0001 C CNN
F 3 "~" H 2700 3100 50  0001 C CNN
	1    2700 3100
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR038
U 1 1 5C63757B
P 2450 3350
F 0 "#PWR038" H 2450 3100 50  0001 C CNN
F 1 "GND" H 2455 3177 50  0000 C CNN
F 2 "" H 2450 3350 50  0001 C CNN
F 3 "" H 2450 3350 50  0001 C CNN
	1    2450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2950 2700 2600
$Comp
L Theremin_controller-rescue:Ferrite_Bead-device FB1
U 1 1 5C63A584
P 3000 1900
F 0 "FB1" V 2726 1900 50  0000 C CNN
F 1 "Ferrite_Bead" V 2817 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2930 1900 50  0001 C CNN
F 3 "~" H 3000 1900 50  0001 C CNN
	1    3000 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 3000 2450 3300
Wire Wire Line
	2700 3250 2700 3300
Wire Wire Line
	2700 3300 2450 3300
Connection ~ 2450 3300
Wire Wire Line
	2450 3300 2450 3350
Wire Wire Line
	2450 2600 2700 2600
Wire Wire Line
	2700 2600 2700 1900
Wire Wire Line
	2700 1900 2850 1900
Connection ~ 2700 2600
Wire Wire Line
	6200 3800 6200 3750
Wire Wire Line
	6000 3800 6200 3800
Wire Wire Line
	3150 1900 3350 1900
$Comp
L Connector:TestPoint TP6
U 1 1 5C66BA24
P 5750 2350
F 0 "TP6" H 5808 2470 50  0000 L CNN
F 1 "TestPoint" H 5808 2379 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5950 2350 50  0001 C CNN
F 3 "~" H 5950 2350 50  0001 C CNN
	1    5750 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2350 5750 2500
Wire Wire Line
	5550 2500 5750 2500
Connection ~ 5750 2500
Wire Wire Line
	5750 2500 6150 2500
$Comp
L Connector:TestPoint TP5
U 1 1 5C66C4E6
P 5650 2150
F 0 "TP5" H 5708 2270 50  0000 L CNN
F 1 "TestPoint" H 5708 2179 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5850 2150 50  0001 C CNN
F 3 "~" H 5850 2150 50  0001 C CNN
	1    5650 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2150 5650 2600
Connection ~ 5650 2600
Wire Wire Line
	5650 2600 6150 2600
$Comp
L Connector:TestPoint TP4
U 1 1 5C66CFF9
P 3000 2750
F 0 "TP4" H 3058 2870 50  0000 L CNN
F 1 "TestPoint" H 3058 2779 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3200 2750 50  0001 C CNN
F 3 "~" H 3200 2750 50  0001 C CNN
	1    3000 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5C66D0B1
P 2900 2550
F 0 "TP3" H 2958 2670 50  0000 L CNN
F 1 "TestPoint" H 2958 2579 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 2550 50  0001 C CNN
F 3 "~" H 3100 2550 50  0001 C CNN
	1    2900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2550 2900 2900
Wire Wire Line
	2450 2900 2900 2900
Connection ~ 2900 2900
Wire Wire Line
	2900 2900 3950 2900
Wire Wire Line
	3000 2750 3000 2800
Connection ~ 3000 2800
Wire Wire Line
	3000 2800 3950 2800
$EndSCHEMATC
