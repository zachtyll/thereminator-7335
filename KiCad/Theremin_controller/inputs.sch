EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "INPUTS"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Theremin_controller-rescue:GP2Y0A02YK0F-Thereminator FrequencySensor?
U 1 1 5C61483C
P 5900 3400
AR Path="/5C61483C" Ref="FrequencySensor?"  Part="1" 
AR Path="/5C60A54E/5C61483C" Ref="S2"  Part="1" 
F 0 "S2" H 5900 3765 50  0000 C CNN
F 1 "GP2Y0A02YK0F" H 5900 3674 50  0000 C CNN
F 2 "ThereminLib:GP2Y0A02YK0F" H 5900 3400 50  0001 C CNN
F 3 "" H 5900 3400 50  0001 C CNN
	1    5900 3400
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GP2Y0A02YK0F-Thereminator AmplitudeSensor?
U 1 1 5C614843
P 5900 2350
AR Path="/5C614843" Ref="AmplitudeSensor?"  Part="1" 
AR Path="/5C60A54E/5C614843" Ref="S1"  Part="1" 
F 0 "S1" H 5900 2715 50  0000 C CNN
F 1 "GP2Y0A02YK0F" H 5900 2624 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5900 2350 50  0001 C CNN
F 3 "" H 5900 2350 50  0001 C CNN
	1    5900 2350
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR?
U 1 1 5C61484A
P 5350 3700
AR Path="/5C61484A" Ref="#PWR?"  Part="1" 
AR Path="/5C60A54E/5C61484A" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 5350 3450 50  0001 C CNN
F 1 "GND" H 5400 3550 50  0000 R CNN
F 2 "" H 5350 3700 50  0001 C CNN
F 3 "" H 5350 3700 50  0001 C CNN
	1    5350 3700
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR?
U 1 1 5C614850
P 5350 2650
AR Path="/5C614850" Ref="#PWR?"  Part="1" 
AR Path="/5C60A54E/5C614850" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 5350 2400 50  0001 C CNN
F 1 "GND" H 5400 2500 50  0000 R CNN
F 2 "" H 5350 2650 50  0001 C CNN
F 3 "" H 5350 2650 50  0001 C CNN
	1    5350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3500 5350 3500
Text HLabel 7200 2350 2    50   Output ~ 0
AMP_INPUT
Wire Wire Line
	6350 2350 6700 2350
Text HLabel 7200 3400 2    50   Output ~ 0
FREQ_INPUT
$Comp
L Theremin_controller-rescue:+5V-power #PWR014
U 1 1 5C61762D
P 4800 2200
F 0 "#PWR014" H 4800 2050 50  0001 C CNN
F 1 "+5V" H 4750 2350 50  0000 L CNN
F 2 "" H 4800 2200 50  0001 C CNN
F 3 "" H 4800 2200 50  0001 C CNN
	1    4800 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2450 5350 2450
$Comp
L Theremin_controller-rescue:+5V-power #PWR016
U 1 1 5C61769C
P 4800 3250
F 0 "#PWR016" H 4800 3100 50  0001 C CNN
F 1 "+5V" H 4750 3400 50  0000 L CNN
F 2 "" H 4800 3250 50  0001 C CNN
F 3 "" H 4800 3250 50  0001 C CNN
	1    4800 3250
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:R-Device R?
U 1 1 5C656E1F
P 6850 3400
AR Path="/5C656E1F" Ref="R?"  Part="1" 
AR Path="/5C60A54E/5C656E1F" Ref="R4"  Part="1" 
F 0 "R4" V 6643 3400 50  0000 C CNN
F 1 "1k" V 6734 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 3400 50  0001 C CNN
F 3 "~" H 6850 3400 50  0001 C CNN
	1    6850 3400
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C?
U 1 1 5C656E26
P 7100 3650
AR Path="/5C656E26" Ref="C?"  Part="1" 
AR Path="/5C60A54E/5C656E26" Ref="C13"  Part="1" 
F 0 "C13" H 6985 3604 50  0000 R CNN
F 1 "330n" H 6985 3695 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7138 3500 50  0001 C CNN
F 3 "~" H 7100 3650 50  0001 C CNN
	1    7100 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 3400 7100 3400
Wire Wire Line
	7100 3400 7100 3500
$Comp
L Theremin_controller-rescue:R-Device R?
U 1 1 5C656F73
P 6850 2350
AR Path="/5C656F73" Ref="R?"  Part="1" 
AR Path="/5C60A54E/5C656F73" Ref="R3"  Part="1" 
F 0 "R3" V 6643 2350 50  0000 C CNN
F 1 "1k" V 6734 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 2350 50  0001 C CNN
F 3 "~" H 6850 2350 50  0001 C CNN
	1    6850 2350
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C?
U 1 1 5C656FAD
P 7100 2600
AR Path="/5C656FAD" Ref="C?"  Part="1" 
AR Path="/5C60A54E/5C656FAD" Ref="C12"  Part="1" 
F 0 "C12" H 6985 2554 50  0000 R CNN
F 1 "330n" H 6985 2645 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7138 2450 50  0001 C CNN
F 3 "~" H 7100 2600 50  0001 C CNN
	1    7100 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 2350 7100 2350
Wire Wire Line
	7100 2350 7100 2450
Wire Wire Line
	7100 2350 7200 2350
Connection ~ 7100 2350
Wire Wire Line
	7100 3400 7200 3400
Connection ~ 7100 3400
$Comp
L Theremin_controller-rescue:GND-power #PWR018
U 1 1 5C6574A5
P 7100 2800
F 0 "#PWR018" H 7100 2550 50  0001 C CNN
F 1 "GND" H 7105 2627 50  0000 C CNN
F 2 "" H 7100 2800 50  0001 C CNN
F 3 "" H 7100 2800 50  0001 C CNN
	1    7100 2800
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR019
U 1 1 5C6574C3
P 7100 3850
F 0 "#PWR019" H 7100 3600 50  0001 C CNN
F 1 "GND" H 7105 3677 50  0000 C CNN
F 2 "" H 7100 3850 50  0001 C CNN
F 3 "" H 7100 3850 50  0001 C CNN
	1    7100 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3850 7100 3800
Wire Wire Line
	7100 2800 7100 2750
Wire Wire Line
	6350 3400 6700 3400
Wire Wire Line
	5350 2450 5350 2650
Wire Wire Line
	4800 2250 4800 2350
Wire Wire Line
	5350 3700 5350 3500
$Comp
L Theremin_controller-rescue:C-device C23
U 1 1 5C6428BE
P 4800 3550
F 0 "C23" H 4915 3596 50  0000 L CNN
F 1 "100n" H 4915 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4838 3400 50  0001 C CNN
F 3 "~" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-device C22
U 1 1 5C642913
P 4800 2500
F 0 "C22" H 4915 2546 50  0000 L CNN
F 1 "100n" H 4915 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4838 2350 50  0001 C CNN
F 3 "~" H 4800 2500 50  0001 C CNN
	1    4800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3250 4800 3300
Wire Wire Line
	4800 3300 5450 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4800 3400
Wire Wire Line
	4800 2250 5450 2250
Connection ~ 4800 2250
Wire Wire Line
	4800 2200 4800 2250
$Comp
L Theremin_controller-rescue:GND-power #PWR?
U 1 1 5C6435C7
P 4800 2650
AR Path="/5C6435C7" Ref="#PWR?"  Part="1" 
AR Path="/5C60A54E/5C6435C7" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 4800 2400 50  0001 C CNN
F 1 "GND" H 4850 2500 50  0000 R CNN
F 2 "" H 4800 2650 50  0001 C CNN
F 3 "" H 4800 2650 50  0001 C CNN
	1    4800 2650
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR?
U 1 1 5C6435E2
P 4800 3700
AR Path="/5C6435E2" Ref="#PWR?"  Part="1" 
AR Path="/5C60A54E/5C6435E2" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 4800 3450 50  0001 C CNN
F 1 "GND" H 4850 3550 50  0000 R CNN
F 2 "" H 4800 3700 50  0001 C CNN
F 3 "" H 4800 3700 50  0001 C CNN
	1    4800 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5C671AB3
P 7100 2200
F 0 "TP1" H 7158 2320 50  0000 L CNN
F 1 "TestPoint" H 7158 2229 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7300 2200 50  0001 C CNN
F 3 "~" H 7300 2200 50  0001 C CNN
	1    7100 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2200 7100 2350
$Comp
L Connector:TestPoint TP2
U 1 1 5C671D3F
P 7100 3250
F 0 "TP2" H 7158 3370 50  0000 L CNN
F 1 "TestPoint" H 7158 3279 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7300 3250 50  0001 C CNN
F 3 "~" H 7300 3250 50  0001 C CNN
	1    7100 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3250 7100 3400
$EndSCHEMATC
