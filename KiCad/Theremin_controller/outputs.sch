EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Audio:OPA1622 U?
U 1 1 5C668B7F
P 6450 3150
F 0 "U?" H 6450 2876 50  0000 C CNN
F 1 "OPA1622" H 6450 2785 50  0000 C CNN
F 2 "Package_SON:Texas_S-PVSON-N10" H 6450 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa1622.pdf" H 6450 3150 50  0001 C CNN
	1    6450 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5C668C32
P 5550 3300
F 0 "C?" H 5665 3346 50  0000 L CNN
F 1 "C" H 5665 3255 50  0000 L CNN
F 2 "" H 5588 3150 50  0001 C CNN
F 3 "~" H 5550 3300 50  0001 C CNN
	1    5550 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C668CAB
P 5800 3050
F 0 "R?" V 5593 3050 50  0000 C CNN
F 1 "R" V 5684 3050 50  0000 C CNN
F 2 "" V 5730 3050 50  0001 C CNN
F 3 "~" H 5800 3050 50  0001 C CNN
	1    5800 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 3150 5550 3050
Wire Wire Line
	5550 3050 5650 3050
Text HLabel 5400 3050 0    50   Input ~ 0
PWM-In
Wire Wire Line
	5550 3050 5400 3050
Connection ~ 5550 3050
$Comp
L power:GND #PWR?
U 1 1 5C669625
P 5550 3550
F 0 "#PWR?" H 5550 3300 50  0001 C CNN
F 1 "GND" H 5555 3377 50  0000 C CNN
F 2 "" H 5550 3550 50  0001 C CNN
F 3 "" H 5550 3550 50  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3550 5550 3450
Wire Wire Line
	5950 3050 6150 3050
Wire Wire Line
	6150 3250 6150 3600
Wire Wire Line
	6150 3600 6750 3600
Wire Wire Line
	6750 3600 6750 3150
Wire Wire Line
	6750 3150 6900 3150
Connection ~ 6750 3150
$Comp
L Connector:AudioJack2 J?
U 1 1 5C66A0B9
P 7300 3150
F 0 "J?" H 7120 3133 50  0000 R CNN
F 1 "AudioJack2" H 7120 3224 50  0000 R CNN
F 2 "" H 7300 3150 50  0001 C CNN
F 3 "~" H 7300 3150 50  0001 C CNN
	1    7300 3150
	-1   0    0    1   
$EndComp
$EndSCHEMATC
