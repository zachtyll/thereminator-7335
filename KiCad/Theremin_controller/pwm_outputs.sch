EESchema Schematic File Version 4
LIBS:Theremin_controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Theremin_controller-rescue:C-Device C19
U 1 1 5C668C32
P 5850 2900
F 0 "C19" H 5965 2946 50  0000 L CNN
F 1 "1u" H 5965 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5888 2750 50  0001 C CNN
F 3 "~" H 5850 2900 50  0001 C CNN
	1    5850 2900
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:R-Device R7
U 1 1 5C668CAB
P 5350 2900
F 0 "R7" V 5143 2900 50  0000 C CNN
F 1 "1k" V 5234 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5280 2900 50  0001 C CNN
F 3 "~" H 5350 2900 50  0001 C CNN
	1    5350 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 2900 5200 2900
Text HLabel 4950 2900 0    50   Input ~ 0
SOUND_PWM
Wire Wire Line
	5100 2900 4950 2900
Connection ~ 5100 2900
$Comp
L Theremin_controller-rescue:GND-power #PWR030
U 1 1 5C669625
P 8150 3600
F 0 "#PWR030" H 8150 3350 50  0001 C CNN
F 1 "GND" H 8155 3427 50  0000 C CNN
F 2 "" H 8150 3600 50  0001 C CNN
F 3 "" H 8150 3600 50  0001 C CNN
	1    8150 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2900 5600 2900
$Comp
L Theremin_controller-rescue:R-Device R8
U 1 1 5C628AD6
P 5650 4500
F 0 "R8" V 5857 4500 50  0000 C CNN
F 1 "220" V 5766 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5580 4500 50  0001 C CNN
F 3 "~" H 5650 4500 50  0001 C CNN
	1    5650 4500
	0    -1   -1   0   
$EndComp
$Comp
L Theremin_controller-rescue:R-Device R10
U 1 1 5C628AE4
P 5650 4900
F 0 "R10" V 5443 4900 50  0000 C CNN
F 1 "220" V 5534 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5580 4900 50  0001 C CNN
F 3 "~" H 5650 4900 50  0001 C CNN
	1    5650 4900
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR031
U 1 1 5C628AF2
P 6700 4700
F 0 "#PWR031" H 6700 4450 50  0001 C CNN
F 1 "GND" H 6705 4527 50  0000 C CNN
F 2 "" H 6700 4700 50  0001 C CNN
F 3 "" H 6700 4700 50  0001 C CNN
	1    6700 4700
	1    0    0    -1  
$EndComp
Text HLabel 5650 5350 0    50   Input ~ 0
BLUE_PWM
Text HLabel 5500 4500 0    50   Input ~ 0
RED_PWM
Wire Wire Line
	6750 3100 6750 3150
$Comp
L Theremin_controller-rescue:+5V-power #PWR034
U 1 1 5C62C620
P 6650 2700
F 0 "#PWR034" H 6650 2550 50  0001 C CNN
F 1 "+5V-power" H 6665 2873 50  0000 C CNN
F 2 "" H 6650 2700 50  0001 C CNN
F 3 "" H 6650 2700 50  0001 C CNN
	1    6650 2700
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:LMH6643-Thereminator U4
U 1 1 5C62CDCA
P 6450 3100
AR Path="/5C62CDCA" Ref="U4"  Part="1" 
AR Path="/5C668B48/5C62CDCA" Ref="U4"  Part="1" 
F 0 "U4" H 7191 3146 50  0000 L CNN
F 1 "LMH6643" H 7191 3055 50  0000 L CNN
F 2 "ti:TI_DGN_S-PDSO-G8" H 6500 3100 50  0001 C CNN
F 3 "" H 6500 3100 50  0001 C CNN
	1    6450 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3100 7350 3100
$Comp
L Theremin_controller-rescue:GND-power #PWR035
U 1 1 5C62DB64
P 6650 3450
F 0 "#PWR035" H 6650 3200 50  0001 C CNN
F 1 "GND-power" H 6655 3277 50  0000 C CNN
F 2 "" H 6650 3450 50  0001 C CNN
F 3 "" H 6650 3450 50  0001 C CNN
	1    6650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3400 6650 3450
Wire Wire Line
	6650 2700 6650 2800
Wire Wire Line
	6000 2900 6050 2900
$Comp
L Theremin_controller-rescue:+5V-power #PWR046
U 1 1 5C643F17
P 5550 3250
F 0 "#PWR046" H 5550 3100 50  0001 C CNN
F 1 "+5V-power" H 5565 3423 50  0000 C CNN
F 2 "" H 5550 3250 50  0001 C CNN
F 3 "" H 5550 3250 50  0001 C CNN
	1    5550 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3300 5550 3300
Wire Wire Line
	5550 3300 5550 3250
$Comp
L Theremin_controller-rescue:R-Device R17
U 1 1 5C644190
P 6400 2350
F 0 "R17" V 6193 2350 50  0000 C CNN
F 1 "1k" V 6284 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 2350 50  0001 C CNN
F 3 "~" H 6400 2350 50  0001 C CNN
	1    6400 2350
	0    1    1    0   
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C24
U 1 1 5C6441D8
P 6200 1950
F 0 "C24" H 6315 1996 50  0000 L CNN
F 1 "15n" H 6315 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 1800 50  0001 C CNN
F 3 "~" H 6200 1950 50  0001 C CNN
	1    6200 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 1950 5600 1950
Wire Wire Line
	5600 1950 5600 2900
Connection ~ 5600 2900
Wire Wire Line
	5600 2900 5700 2900
Wire Wire Line
	6250 2350 6050 2350
Wire Wire Line
	6050 2350 6050 2900
Connection ~ 6050 2900
Wire Wire Line
	6050 2900 6150 2900
Wire Wire Line
	6550 2350 7350 2350
Wire Wire Line
	7350 2350 7350 3100
Connection ~ 7350 3100
Wire Wire Line
	6350 1950 7350 1950
Wire Wire Line
	7350 1950 7350 2350
Connection ~ 7350 2350
$Comp
L Theremin_controller-rescue:+5V-power #PWR050
U 1 1 5C62CCE3
P 9100 1900
F 0 "#PWR050" H 9100 1750 50  0001 C CNN
F 1 "+5V-power" H 9115 2073 50  0000 C CNN
F 2 "" H 9100 1900 50  0001 C CNN
F 3 "" H 9100 1900 50  0001 C CNN
	1    9100 1900
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C25
U 1 1 5C62CD09
P 9100 2050
F 0 "C25" H 9215 2096 50  0000 L CNN
F 1 "100n" H 9215 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9138 1900 50  0001 C CNN
F 3 "" H 9100 2050 50  0001 C CNN
	1    9100 2050
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR051
U 1 1 5C62CD4B
P 9100 2350
F 0 "#PWR051" H 9100 2100 50  0001 C CNN
F 1 "GND" H 9105 2177 50  0000 C CNN
F 2 "" H 9100 2350 50  0001 C CNN
F 3 "" H 9100 2350 50  0001 C CNN
	1    9100 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2200 9100 2350
NoConn ~ 5850 4700
$Comp
L Theremin_controller-rescue:AudioJack3_SwitchTR-Connector J3
U 1 1 5C646475
P 8400 3400
AR Path="/5C646475" Ref="J3"  Part="1" 
AR Path="/5C668B48/5C646475" Ref="J3"  Part="1" 
F 0 "J3" H 8120 3233 50  0000 R CNN
F 1 "AudioJack3_SwitchTR" H 8120 3324 50  0000 R CNN
F 2 "ThereminLib:Audio_Jack_3.5mm" H 8400 3400 50  0001 C CNN
F 3 "~" H 8400 3400 50  0001 C CNN
	1    8400 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 3500 8150 3500
Wire Wire Line
	8150 3500 8150 3600
Wire Wire Line
	7350 3100 7850 3100
Wire Wire Line
	8200 3300 8000 3300
Wire Wire Line
	8000 3300 8000 3100
Connection ~ 8000 3100
Wire Wire Line
	8000 3100 8200 3100
Wire Wire Line
	8200 3400 8150 3400
Wire Wire Line
	8150 3400 8150 3500
Connection ~ 8150 3500
Wire Wire Line
	8200 3200 8150 3200
Wire Wire Line
	8150 3200 8150 3400
Connection ~ 8150 3400
Wire Wire Line
	5800 4500 5850 4500
Wire Wire Line
	5800 4900 5850 4900
$Comp
L Theremin_controller-rescue:LED_RGB-Device D1
U 1 1 5C646457
P 6050 4700
F 0 "D1" H 6050 4233 50  0000 C CNN
F 1 "LED_RGB-Device" H 6050 4324 50  0000 C CNN
F 2 "ThereminLib:SMD_LED" H 6050 4650 50  0001 C CNN
F 3 "" H 6050 4650 50  0001 C CNN
	1    6050 4700
	-1   0    0    1   
$EndComp
Text Notes 6150 4250 0    50   ~ 0
Schematic wrong, B and R pins flipped
NoConn ~ 6250 4700
Wire Wire Line
	6250 4500 6700 4500
Wire Wire Line
	6700 4500 6700 4700
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 5C648208
P 6600 5350
F 0 "Q1" H 6791 5396 50  0000 L CNN
F 1 "2N2219" H 6791 5305 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6800 5275 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 6600 5350 50  0001 L CNN
	1    6600 5350
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:GND-power #PWR055
U 1 1 5C6493CC
P 6700 5700
F 0 "#PWR055" H 6700 5450 50  0001 C CNN
F 1 "GND" H 6705 5527 50  0000 C CNN
F 2 "" H 6700 5700 50  0001 C CNN
F 3 "" H 6700 5700 50  0001 C CNN
	1    6700 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5550 6700 5700
$Comp
L Theremin_controller-rescue:+5V-power #PWR053
U 1 1 5C649A57
P 4800 4750
F 0 "#PWR053" H 4800 4600 50  0001 C CNN
F 1 "+5V-power" H 4815 4923 50  0000 C CNN
F 2 "" H 4800 4750 50  0001 C CNN
F 3 "" H 4800 4750 50  0001 C CNN
	1    4800 4750
	1    0    0    -1  
$EndComp
$Comp
L Theremin_controller-rescue:C-Device C29
U 1 1 5C64B182
P 4800 5100
F 0 "C29" H 4915 5146 50  0000 L CNN
F 1 "100n" H 4915 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4838 4950 50  0001 C CNN
F 3 "" H 4800 5100 50  0001 C CNN
	1    4800 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4750 4800 4900
Connection ~ 4800 4900
Wire Wire Line
	4800 4900 4800 4950
$Comp
L Theremin_controller-rescue:GND-power #PWR054
U 1 1 5C64BA07
P 4800 5400
F 0 "#PWR054" H 4800 5150 50  0001 C CNN
F 1 "GND" H 4805 5227 50  0000 C CNN
F 2 "" H 4800 5400 50  0001 C CNN
F 3 "" H 4800 5400 50  0001 C CNN
	1    4800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5250 4800 5400
$Comp
L Theremin_controller-rescue:R-Device R19
U 1 1 5C648A80
P 6100 5350
F 0 "R19" V 5893 5350 50  0000 C CNN
F 1 "10k" V 5984 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6030 5350 50  0001 C CNN
F 3 "~" H 6100 5350 50  0001 C CNN
	1    6100 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 5350 6400 5350
Wire Wire Line
	5950 5350 5650 5350
Wire Wire Line
	6250 4900 6700 4900
Wire Wire Line
	6700 4900 6700 5150
Wire Wire Line
	4800 4900 5500 4900
$Comp
L Connector:TestPoint TP7
U 1 1 5C672CA6
P 5100 2600
F 0 "TP7" H 5158 2720 50  0000 L CNN
F 1 "TestPoint" H 5158 2629 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5300 2600 50  0001 C CNN
F 3 "~" H 5300 2600 50  0001 C CNN
	1    5100 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5C672D46
P 7850 2900
F 0 "TP8" H 7908 3020 50  0000 L CNN
F 1 "TestPoint" H 7908 2929 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8050 2900 50  0001 C CNN
F 3 "~" H 8050 2900 50  0001 C CNN
	1    7850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 2900 7850 3100
Connection ~ 7850 3100
Wire Wire Line
	7850 3100 8000 3100
Wire Wire Line
	5100 2600 5100 2900
$EndSCHEMATC
