# Therminator 7335

Thermin for the therminator <3
## Grades

### 3
- ADC           - Anton
- PWM           - Zacharias
- UART          - Martin
- DIP switches  - Oskar

### 4 och 5
- MIDI                  - Zacharias
- DMA                   - Oskar
- Low Power Modes       - Anton
- KLEE verification     - Martin

## Introduction to the project
The purpose of this project was to design and implement a Theremin on a PCB. This project was made for us to learn more about embedded programming.

## Recreation of our tests
<h4>How to test and run the ADC</h4>
To test the ADC, you simply need a bread board, three cables, and some resistors. Use the ground and the 5V-pin from the nucleo, too get zero milivolts in the tests a 100 ohm 
resistor should be plugged to the ground, and a 10 Mohm resistor to the 5V. Then to test different input voltages, simply use the same resistor for the ground at each test,
and different ones for the 5V. I used 100 kOhm resistors for the ground and then from 100k Ohm up to 1 Mohm for the 5V.

To run the code, open the terminal and navigate to the thereminator-7335 folder. To compile, run
``` console
$ cargo build --example adc --features "hal rtfm-tq"
```
And for debuging on the STM, run
``` console
$ arm-none-eabi-gdb target/thumbv7em-none-eabihf/debug/examples/adc -x openocd.gdb
```
Your ITM dump should print different voltages depending on the different combinations of resistors.

### How to test and run the UART Communication
A Nucleo board is all you need!
``` console
cargo build --example uart --features "hal rtfm-tq"
```
``` console
arm-none-eabi-gdb target/thumbv7em-none-eabihf/debug/examples/uart -x openocd.gdb
```
The uart output can then be reada using your favourite COM-port terminal software (moserial, for example) the uart demo outputs "MIDI" once per second


### How to test and run the PWM
A Nucleo board is all you need. Should be compatible with STM32F4xx, built on STM32F41RE.
``` console
$ $ cargo build --example pwmTest --features "hal rtfm"
```
And then for running:
``` console
$ $ arm-none-eabi-gdb target/thumbv7em-none-eabihf/debug/examples/pwmTest -x openocd.gdb
```

The PWM should be able to be read from the D7 pin (PA8) with an oscilloscope. Remember to connect to the ground!
Adjust PSC, ARR and CRR1 by what you want the waveform to look like.

## Known limitations and future work
The first thing that comes to mind is to actually have a PCB that does not have any faults.

Another thing that can be done in the future, is to create a task out of the ADC sampling so it can be called upon several times. Right now the code for the sampling is in the init,
which means that it will only sample one time. There is a begining on said task, however the problem was that the recourse handling did not go too well.

The functionality from grade 4 and 5 could also be implemented in future work.

## Setting up RUST for Windows 10

### Dependencies


- Clone the repo here(link).

- Rust 1.32, or later.

- `rust-std` components (pre-compiled `core` crate) for the ARM Cortex-M
  targets. Run:

``` console
$ rustup target add thumbv7em-none-eabihf
```

- For programming (flashing) and debugging
  - `openocd` (install using your package manager)
  - `arm-none-eabi` toolchain (install using your package manager). In the following we refer the `arm-none-eabi-gdb` as just `gdb` for brevity.

- `st-flash` (for low level access to the MCU flash)
- `itmdump` (for ITM trace output)
- `vscode` and `cortex-debug` (optional for an integrated debugging experience)

* https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug

### OpenOCD
Download OpenOCD from here(link),  x32 works for x64. Install the program. Make sure that we have OpenOCD in the `$PATH$` environment variable by running

``` console
$ $env:Path -split ';'
```
Look for `...\GNU MCU Eclipse\OpenOCD\0.10.0-11-20190118-1134\bin`. We can now check that openOCD was correctly installed by running
    
``` console
$ openOCD -v
```

### GDB
Download GDB from here(link). Follow the default steps of the installer. Check wether GDB is in our `$PATH$` environment variable
``` console
$ $env:Path -split ';'
``` 
Look for `...\GNU Tools ARM Embedded\8 2018-q4-major\bin`.
    
Test if it was installed correctly by running.
``` console
$ arm-none-eabi-gdb -v
```
    
### ST-Link
Get ST-Link software from here(link).
    
### RUST
    
Get rustup from here(link). Run the .exe and follow default steps for install. Before starting a new development session always run
``` console
$ rustup update
```
This may take some time.
    
Add a new target `thumbv7em-none-eabihf`
``` console
$ rustup target add thumbv7em-none-eabihf
```
NOTE: You can always run rustup doc if you need additional help.
    
### Connect to STM32Nucleo
Connect the STM32Nucleo into a USB-port. Run
``` console
$ gwmi Win32_USBControllerDevice |%{[wmi]($_.Dependent)} | Sort Manufacturer,Description,DeviceID | Ft -GroupBy Manufacturer Description
```
The ST-link should show up and the LED on the Nucleo glow red.
    
Open a new Powershell and navigate to the folder `...\e7020e_2019`. Run
``` console
$ openocd -f openocd.cfg
```
This should cause the Nucleo to start flashing Green/Red.
    
### Building and Running
Open up a new Powershell and run
``` console
$ cargo build --example hello
$ arm-none-eabi-gdb target/thumbv7em-none-eabihf/debug/examples/hello -x openocd.gdb
```
We should now be debugging the program with GDB.
Writing `c` will continue the program until it reaches a predefined breakpoint and
writing `n` will continue the program one step.
writing `q` will prompt you to quit GDB.

### Using ITM tracing on Win10
Open a new Powershell and run
``` console
$ cargo install itm
```
Now open the file `openocd.gdb` and change the line `monitor tpiu config internal /tmp/log.log uart off 16000000`
by replacing `/tmp/log.log` with `ITMdump`.

Go to the shell that we ran `arm-none-eabi-gdb target/thumbv7em-none-eabihf/debug/examples/hello -x openocd.gdb` and
change `hello` to `itm`. Run the program. This should create a file named `ITMdump`. 
Switch to the shell we installed `itm` in and write
``` console
$ itmdump -f .\ITMdump -F
```
The contents of the file `ITMdump` should now show up in the terminal. If this does not work we might be in the wrong
folder.
    
