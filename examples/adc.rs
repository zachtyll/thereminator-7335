//! The RTFM framework
//!
//! What it covers:
//! - Priority based scheduling
//! - Message passing

#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m::{asm, iprintln};

extern crate stm32f4xx_hal as hal;
use crate::hal::prelude::*;
use crate::hal::serial::{config::Config, Event, Rx, Serial, Tx};
use hal::stm32::ITM;

use nb::block;
use rtfm::{app, Instant};
use stm32f4xx_hal::{
  gpio::gpioa,
  adc::{
    Adc,
    config::AdcConfig,
    config::SampleTime,
  },
};

// Our error type
#[derive(Debug)]
pub enum Error {
    RingBufferOverflow,
    UsartSendOverflow,
    UsartReceiveOverflow,
}

//For getting our MVF
fn average(numbers: &[i32]) -> f32 {
    numbers.iter().sum::<i32>() as f32 / numbers.len() as f32
}

#[app(device = hal::stm32)]
const APP: () = {
    // Late resources
    static mut ITM: ITM = ();
    //static mut DEVICE: hal::stm32::Peripherals = ();
    //static mut DEVICEADC1: hal::stm32::ADC1 = ();
    //static mut GPIO: hal::gpio::gpioa::Parts = ();
    //static mut INPUT: stm32f4xx_hal::stm32::GPIOA = ();

    // init runs in an interrupt free section>
    #[init]
    fn init() {
        let stim = &mut core.ITM.stim[0];
        iprintln!(stim, "adc");

        let rcc = device.RCC.constrain();

        // 16 MHz (default, all clocks)
        let clocks = rcc.cfgr.freeze();
 
        let gpioa = device.GPIOA.split();
        let mut adc = Adc::adc1(device.ADC1, true, AdcConfig::default());
        let pa0 = gpioa.pa0.into_analog();
        let sample = adc.convert(&pa0, SampleTime::Cycles_480);
        let millivolts = adc.sample_to_millivolts(sample);
        
        iprintln!(stim, "millivolt {:?}", millivolts);
        // Resources
        ITM = core.ITM;
        //DEVICE = device;
        //DEVICEADC1 = device.ADC1;
        //GPIO = gpioa;
    }

 /*    #[task(priority = 1, resources = [ITM, DEVICEADC1, GPIO])]
        fn getSample(error: Error) {
        let temp = &*resources.DEVICEADC1;
        let mut adc = Adc::adc1(*temp, true, AdcConfig::default());
        let pa0 = resources.GPIO.pa0.into_analog();
        let sample = adc.convert(&pa0, SampleTime::Cycles_480);
        let millivolts = adc.sample_to_millivolts(sample);
        
     let mut adc = Adc::adc1(device.ADC1, true, AdcConfig::default());
        let pa3 = gpioa.pa3.into_analog();
        let sample = adc.convert(&pa3, SampleTime::Cycles_480);
        let millivolts = adc.sample_to_millivolts(sample);
        info!("pa3: {}mV", millivolts);
    } */

    // idle may be interrupted by other interrupt/tasks in the system
    #[idle]
    fn idle() -> ! {
        loop {
            asm::wfi();
        }
    }

    #[task(priority = 2, resources = [ITM], capacity = 4)]
    fn trace_data(byte: u8) {
        let stim = &mut resources.ITM.stim[0];
        iprintln!(stim, "data {}", byte);
    }

    #[task(priority = 2, resources = [ITM])]
    fn trace_error(error: Error) {
        let stim = &mut resources.ITM.stim[0];
        iprintln!(stim, "{:?}", error);
    }

    // Set of interrupt vectors, free to use for RTFM tasks
    // 1 per priority level suffices
    extern "C" {
        fn EXTI0();
        fn EXTI1();
        fn EXTI2();
    }
};
