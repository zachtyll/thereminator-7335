//! bare3.rs
//!
//! String types in Rust
//!
//! What it covers:
//! - Types, str, arrays ([u8;uszie]), slices (&[u8])
//! - Iteration, copy
//! - Semihosting (tracing)

#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m_rt::entry;
use cortex_m_semihosting::{hprint, hprintln};

#[entry]
fn main() -> ! {
    hprintln!("bare3").unwrap();
    let s: &str = "ABCD";
    let bs: &[u8] = s.as_bytes();

    hprintln!("s = {}", s).unwrap();
    hprintln!("bs = {:?}", bs).unwrap();

    hprintln!("iterate over slice").unwrap();
    for c in bs {
        // let c: u8 = c;
        hprint!("{},", c).unwrap();
    }

    hprintln!("iterate iterate using (raw) indexing").unwrap();
    for i in 0..s.len() {
        let i: usize = i;
        hprintln!("{},", bs[i]).unwrap();
    }

    hprintln!("").unwrap();

    let mut a: [u8; 4] = [0u8; 4];
    for i in 0..bs.len() {
        a[i] = bs[i];
    }

    hprintln!("").unwrap();
    hprintln!("a = {}", core::str::from_utf8(&a).unwrap()).unwrap();

    let a = &bs[..];

    hprintln!("").unwrap();
    hprintln!("a = {}", core::str::from_utf8(&a).unwrap()).unwrap();

    loop {}
}

// 0. Build and run the application (debug build).
//
//    > cargo build --example bare3
//    (or use the vscode build task)
//
// 1. What is the output in the `openocd` (Adapter Output) console?
//
//    bare3
//    s = ABCD
//    bs = [65,66,67,68]
//    iterate over slice
//    65,66,67,68,iterate iterate using (raw) indexing
//    65,
//    66,
//    67,
//    68,
//
//
//    a = AAAA
//
//    What is the type of `s`?
//
//    &str
//
//    What is the type of `bs`?
//
//    byte array &[u8]
//
//    What is the type of `c`?
//
//    byte (u8)?
//
//    What is the type of `a`?
//
//    u8 array [u8; 4]
//
//    What is the type of `i`?
//
//    u32?
//
//    Commit your answers (bare3_1)
//
// 2. Make types of `s`, `bs`, `c`, `a`, `i` explicit.
//
//    Oh no! c is a reference.
//
//    Commit your answers (bare3_2)
//
// 3. Uncomment line `let mut a = [0u8; 4];
//`
//    Run the program, what happens and why?
//
//    Warning because a is mut. Print is printing as character and does not show anything because
//    0x00 is not a printable unicode character.
//
//    Commit your answers (bare3_3)
//
// 4. Alter the program so that the data from `bs` is copied byte by byte into `a`.
//
//    Test that it works as intended.
//
//    Commit your answers (bare3_4)
//
// 5. Look for a way to make this copy done without a loop.
//    https://doc.rust-lang.org/std/primitive.slice.html
//
//    Implement and test your solution.
//
//    Commit your answers (bare3_5)
