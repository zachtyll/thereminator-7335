//! This example should work to send MIDI data via serial communication

#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m::{asm, iprintln};

extern crate stm32f4xx_hal as hal;
use crate::hal::prelude::*;
use crate::hal::serial::{config::Config, Serial, Tx};
use hal::stm32::ITM;

use nb::block;
use rtfm::{app, Instant};

// Our error type
#[derive(Debug)]
pub enum Error {
    UsartSendOverflow,
}

const PERIOD: u32 = 16_000_000;
#[app(device = hal::stm32)]
const APP: () = {
    // Late resources
    static mut TX: Tx<hal::stm32::USART2> = ();
    static mut ITM: ITM = ();

    // init runs in an interrupt free section>
    #[init(spawn = [dummy_midi])]
    fn init() {
        let stim = &mut core.ITM.stim[0];
        iprintln!(stim, "bare10");

        let rcc = device.RCC.constrain();

        // 16 MHz (default, all clocks)
        let clocks = rcc.cfgr.freeze();

        let gpioa = device.GPIOA.split();

        let tx = gpioa.pa2.into_alternate_af7();
        let rx = gpioa.pa3.into_alternate_af7(); // try comment out

        let serial = Serial::usart2(
            device.USART2,
            (tx, rx),
            Config::default().baudrate(115_200.bps()),
            clocks,
        )
        .unwrap();

        // Separate out the sender and receiver of the serial port
        let (tx, _rx) = serial.split();

        spawn.dummy_midi().unwrap();

        // Our split serial
        TX = tx;

        // For debugging
        ITM = core.ITM;
    }

    // idle may be interrupted by other interrupt/tasks in the system
    #[idle]
    fn idle() -> ! {
        loop {
            asm::wfi();
        }
    }

    #[task(priority = 2, resources = [TX], spawn = [trace_error], capacity = 4)]
    fn usart_tx(byte: u8) {
        let tx = resources.TX;
        if block!(tx.write(byte)).is_err() {
            spawn.trace_error(Error::UsartSendOverflow).unwrap();
        }
    }

    #[task(priority = 1, resources = [ITM])]
    fn trace_error(error: Error) {
        let stim = &mut resources.ITM.stim[0];
        iprintln!(stim, "{:?}", error);
    }

    // Midi communication is not implemented yet, so a dummy task is implemented to simulate the
    // midi transfer.

    #[task(priority = 2, spawn = [usart_tx], schedule = [dummy_midi] )]
    fn dummy_midi() {
        const DUMMY_MIDI_COMMAND: [char; 4] = ['M', 'I', 'D', 'I'];

        for byte in DUMMY_MIDI_COMMAND.iter() {
            spawn.usart_tx(*byte as u8).unwrap();
        }
        schedule
            .dummy_midi(Instant::now() + PERIOD.cycles())
            .unwrap();
    }

    // Set of interrupt vectors, free to use for RTFM tasks
    // 1 per priority level suffices
    extern "C" {
        fn EXTI0();
        fn EXTI1();
        fn EXTI2();
    }
};
