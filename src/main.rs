// late Res
    // static mut ITM: ITM = ();

    // #[init]
    // fn init() {
    //     let stim = &mut core.ITM.stim[0];
    //     iprintln!(stim, "MainPWM");
    //     // For debugging
        
    //     // COUNTER_Frequency = PWM_Frequency * PWM_Steps = 100 * 100 = 10000 = 10KHz
    //     // TIMER_Prescale = (TIMER_Frequency / COUNTER_Frequency) – 1 = 72000000/10000 – 1 = 7199
    //     // let _peripherals = stm32f4::stm32f413::Peripherals::take().unwrap();

    //     let peripherals = hal::stm32::Peripherals::take().unwrap();
    //     let tim4 = &peripherals.TIM4;
    //     tim4.ccr1.modify(|_, w| w.ccr1().bits(0b110));
    //     tim4.arr.modify(|_, w| w.arr().bits(0b100));
    //     let ccr1_test = tim4.ccr1.read();
    //     ITM = core.ITM;
    // }


#![no_main]
#![no_std]


// panic-handler crate
extern crate panic_halt;
extern crate stm32f4xx_hal as hal;


use crate::hal::serial::{config::Config, Rx, Serial, Tx};
use crate::hal::prelude::*;

use stm32f4xx_hal::gpio::gpioa::PA8;
use stm32f4xx_hal::gpio::AF0;
use stm32f4xx_hal::gpio::Alternate;

use hal::stm32::{ITM, USART2, TIM1, DWT};
use cortex_m::{asm, iprintln};
use rtfm::app;
use nb::block;


#[app(device = hal::stm32)]
const APP: () = {
    // Late resources
    static mut TX: Tx<USART2> = ();
    static mut RX: Rx<USART2> = ();
    static mut ITM: ITM = ();
    static mut TIM1: TIM1 = ();
    static mut DWT: DWT = ();
    // static mut LED2: PA8<Alternate<AF0>> = ();
    static mut LED: hal::gpio::gpioa::PA5<hal::gpio::Output<hal::gpio::PushPull>> = ();


    // init runs in an interrupt free section
    #[init]
    fn init() {
        let stim = &mut core.ITM.stim[0];
        iprintln!(stim, "PWM");

        let tim1 = device.TIM1;

        //RCC Unconstrained
        let rcc = device.RCC;
        rcc.apb2enr.write(|w| w.tim1en().enabled());

        let dwt = core.DWT;
        
        // 16 MHz (default, all clocks)
        let clocks = rcc.constrain().cfgr.freeze();

        let gpioa = device.GPIOA.split();
        let tx = gpioa.pa2.into_alternate_af7();
        let rx = gpioa.pa3.into_alternate_af7();
        let led = gpioa.pa5.into_push_pull_output();

        gpioa.pa8.into_alternate_af1();

        //tim1.ccmr1_output.write(|w| w.oc1pe().set_bit()); // Sets the preload register to enabled.
        //gpioa.pa8.into_alternate_af1();
        // let led2 = gpioa.pa8.into_alternate_af0();
        // unsafe {tim1.egr.write(|w| w.ug().update())}            // Updates the registers. Must be done before enabling TIM1.
        tim1.ccer.write(|w| w.cc1e().set_bit());
        tim1.bdtr.write(|w| w.moe().set_bit());                 // Activate Main output Enable
        unsafe{ tim1.ccmr1_output.write(|w| w.oc1m().bits(110))}; // Sets output to PWM: mode 1
        // unsafe{ tim1.cr1.write(|w| w.arpe().set_bit())};            // Sets the auto-reload preloader to enabled.
        // DO NOT TRUST THE DOCS! THIS MUST BE OFF FOR THE PWM TO WORK, WE DONT KNOW WHY!
        // unsafe {tim1.egr.write(|w| w.ug().update())}            // Updates the registers. Must be done before enabling TIM1.

        tim1.cr1.modify(|_, w| w.cen().set_bit());              // Enable tim1
        unsafe {tim1.egr.write(|w| w.ug().update())}            // Updates the registers. Must be done before enabling TIM1.
        
        unsafe {tim1.psc.modify(|_, w| w.psc().bits(0x00FF))}; //Prescaler
        unsafe {tim1.ccr1.modify(|_, w| w.ccr1().bits(0x0080))}; //Threshold
        unsafe {tim1.arr.modify(|_, w| w.arr().bits(0x00FF))};  // Number of steps before reset / Freq

        let serial = Serial::usart2(
            device.USART2,
            (tx, rx),
            Config::default().baudrate(115_200.bps()),
            clocks,
        )
        .unwrap();

        // Separate out the sender and receiver of the serial port
        let (tx, rx) = serial.split();

        // Late resources
        TX = tx;
        RX = rx;
        ITM = core.ITM;
        TIM1 = tim1;
        DWT = dwt;
        LED = led;
        // LED2 = led2;
    }

    // idle may be interrupted by other interrupt/tasks in the system
    #[idle(resources = [RX, TX, ITM, TIM1, DWT, LED])]
    fn idle() -> ! {
        let rx = resources.RX;
        let tx = resources.TX;
        let stim = &mut resources.ITM.stim[0];
        let tim1 = resources.TIM1;
        let mut recieved = 0;
        let mut errors = 0;
        let led = resources.LED;
        // let led2 = resources.LED2;

        iprintln!(stim, "Step 3 ok");
        iprintln!(stim, "Step 2 ok");

            // TODO: Figure out how to rout LED with ODR and GPIOA and BS5
        // gpioa.moder.modify(|_, w| w.moder5().output());


        // ARR = Frequency: pwm steps
        // CCRX = DUTY-CYCLE
        // CNT = The clock value register

        // tim1.cr1.write(|w| w.dir().bit(false)); // Set direction UP.

        // unsafe {tim1.cnt.modify(|_, w| w.cnt().bits(0x0000))};  // Counter is set to zero for new run.
      
        // iprintln!(stim, "psc: {}", tim1.psc.read().bits());
        
        // unsafe {tim1.cr2.modify(|_, w| w.mms().bits(0b0000))}; // Set OC1REF till CNT eller något..
        // unsafe {tim1.ccmr1_output.modify(|_, w| w.oc1m().bits(0b110))} // Set CH1 som output, PWM mode 1.
        // tim1.ccer.modify(|_, w| w.cc1e().set_bit());        // Sätt på timer

        // let dwt = resources.DWT;
        // dwt.enable_cycle_counter();
        // unsafe {
        //     dwt.cyccnt.write(0);
        // }
        // let t = DWT::get_cycle_count();
        // iprintln!(stim, "{}", t);

        loop {
            // match block!(rx.read()) {
                // Ok(byte) => {
                // let tim1_cnt = tim1.cnt.read().bits();
                //     iprintln!(stim, "CNT: {}", tim1.cnt.read().bits());

                // led.set_low();
                // }
                // else {
                //     led.set_high();
                // }

                    // recieved += 1;
                    // iprintln!(stim, "Ok {:?}, received= {:?}", byte, recieved);
                    // tx.write(byte).unwrap();
                // }
                // Err(err) => {
                    // errors += 1;
                    // iprintln!(stim, "Error {:?}, errors= {:?}", err, errors);
                // }
            // }
        }
    }
};